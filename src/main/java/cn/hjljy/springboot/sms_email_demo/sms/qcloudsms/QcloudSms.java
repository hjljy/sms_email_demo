package cn.hjljy.springboot.sms_email_demo.sms.qcloudsms;

import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import org.json.JSONException;

import java.io.IOException;

/**
 * @author yichaofan
 * @date 2020/1/3 17:21
 * @apiNote //TODO
 */
public class QcloudSms {

    public static void main(String[] args) {
        int appid = 1400301910;

        String appkey = "afac387fbf97e389ad75b43a329ba7f2";

        String phoneNumbers = "18628272805";

        /**
         * 短信模板ID  需要在短信应用中申请
         */
        int templateId = 511952;
        /**
         *签名 NOTE: 签名参数使用的是`签名内容`，而不是`签名ID`。
         */
        String smsSign = "海加尔一望无垠的大海网";
        try {
            String[] params = {"5678"};
            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);
            SmsSingleSenderResult result = ssender.sendWithParam("86", phoneNumbers,
                    templateId, params, smsSign, "", "");
            System.out.println(result);
        } catch (HTTPException e) {
            // HTTP 响应码错误
            e.printStackTrace();
        } catch (JSONException e) {
            // JSON 解析错误
            e.printStackTrace();
        } catch (IOException e) {
            // 网络 IO 错误
            e.printStackTrace();
        }
    }
}
