package cn.hjljy.springboot.sms_email_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsEmailDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmsEmailDemoApplication.class, args);
    }

}
