package cn.hjljy.springboot.sms_email_demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@SpringBootTest
class SmsEmailDemoApplicationTests {

    @Autowired
    JavaMailSenderImpl javaMailSender;

    @Test
    void contextLoads() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("921244819@qq.com");
        message.setTo("hjljy@outlook.com");
        message.setSubject("邮件主题");
        message.setText("邮件内容信息测试");
        javaMailSender.send(message);
    }

}
